#ifndef HAMACOVER_H
#define HAMACOVER_H

typedef uint8_t* NETWORK;

typedef struct PAIR {
    uint8_t a;
    uint8_t b;
} PAIR ;

/**
 * Prints help.
 */
void print_usage();

/**
 *  Based on the number of players p and the networks size n,
 *  construct the networks neccessary to ensure that for every two
 *  players p1 and p2 there exists at least one network n in which
 *  both players are a member in.
 *
 *  The networks are constructed using a lexicographic greedy
 *  approximation, thus the number of networks returne is not always minimal.
 *
 *  @param p Number of player
 *  @param n Size of the networks
 *  @param nn Contains the number of networks returned
 *  @return An array of networks. Each network is represented by an array of
 *  integers from 1 to p, denoting the resprective player. The size of every
 *  network is n. If a networks is not completeley filled with players, it is
 *  padded with zeros.
 */
NETWORK *construct_network_cover(uint8_t p, uint8_t n, uint64_t *nn);

/**
 *  Calculates the binomial coefficient n over k.
 */
uint64_t choose(uint64_t n, uint64_t k);

/**
 *  Calculates the greatest common divisor using Euclid's algorithm.
 */
uint64_t gcd(uint64_t, uint64_t);

/**
 * Constructs a list of all possible networks of size r when choosing
 * out of (end-start) persons. The list constructed is in lexicographical order.
 *
 * @param *all Acts as the buffer which is filled with the list of networks.
 * It is expected to have dimensions all[*c_idx+choose(end-start,r)][r].
 * @param *buf A buffer storing the current network in construction. Buf is
 * expected to have length r.
 * @param c_idx Stores the current index of all which will be used to reference
 * buf to. This should be 0 when this method is called initially.
 * @param start The smallest person ID used
 * @param end The largest person ID used
 * @param r The size of each network
 */
void compute_combinations(NETWORK *all, NETWORK buf, uint64_t *c_idx,
                uint8_t start, uint8_t end, uint8_t index, uint8_t r);

PAIR *get_network_pairs(NETWORK network, uint8_t n_size);

PAIR *merge_pair_list(PAIR *a, uint64_t a_size, PAIR *b, uint64_t b_size);

uint64_t count_new_pairs(PAIR *src, uint64_t src_size,
                PAIR *new_p, uint64_t new_size);

int8_t compare_pairs(PAIR a, PAIR b);

#endif /* HAMACOVER_H */

