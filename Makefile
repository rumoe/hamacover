CC=gcc
CFLAGS=-I . -Wall -Wextra
DEPS = hamacover.h
OBJ = hamacover.o

%.o: %.c $(DEPS)
		$(CC) -c -o $@ $< $(CFLAGS)

hamacover: $(OBJ)
		gcc -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o *.exe hamacover
