#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <inttypes.h>

#include "hamacover.h"

void print_usage() {
    printf("usage: hamacover player_number network_size [-h]\n");
}

int main(int argc, char *argv[]) {
    int c, index;
    long int player_num, network_size;
    int errflg = 0;

    // handle flags
    while ((c = getopt(argc, argv, ":h")) != -1) {
        switch (c) {
        case 'h':
            print_usage();
            exit(EXIT_SUCCESS);
        case ':':
            fprintf(stderr, "Option -%c requires an operand.\n", optopt);
            errflg++;
            break;
        case '?':
            fprintf(stderr, "Dafuq is '-%c'? RTFM (-h)!\n", optopt);
            errflg++;
            break;
        }
    }
    if (errflg) {
        exit(EXIT_FAILURE);
    }
    // handle non optional arguments
    index = optind;
    if (argc - index != 2) {
        fprintf(stderr, "Wrong number of arguments.\n");
        print_usage();
        exit(EXIT_FAILURE);
    }
    player_num = strtol(argv[index++], NULL, 10);
    network_size = strtol(argv[index], NULL, 10);

    if (player_num < 2 || player_num > 255
            || network_size < 2 || network_size > 255) {
        fprintf(stderr, "Network size and player number must be integer "
                "between 2 and 255 inclusive.\n");
        exit(EXIT_FAILURE);
    }

    // construct cover and display it
    uint64_t network_num;
    NETWORK *networks = construct_network_cover((uint8_t) player_num,
            (uint8_t) network_size, &network_num);
    if (!networks) {
        fprintf(stderr, "Oops! Something happened...\n");
        exit(EXIT_FAILURE);
    }

    // cheap console print
    unsigned int ni, pi;
    for (ni = 0; ni < network_num; ni++) {
        printf("N %d: { ", ni+1);
        NETWORK network = networks[ni];
        for (pi = 0; pi < network_size; pi++) {
            if (network[pi] == 0) break;
            printf("%d ", (int) network[pi]);
        }
        printf("}\n");
    }
    // free all the shit
    if (networks) {
        uint32_t i;
        for (i = 0; i < network_num; i++) {
            free(networks[i]);
        }
        free(networks);
    }
    exit(EXIT_SUCCESS);
}

NETWORK *construct_network_cover(uint8_t p_num, uint8_t n_size, uint64_t *n_num) {
    printf("Constructing networks...\n");
    if (p_num <= n_size) {
        // if there are less players than the size of a network
        // stuff everyone in the same
        NETWORK *networks = malloc(sizeof(NETWORK));
        networks[0] = malloc(sizeof(uint8_t) * n_size);

        memset(networks[0], 0, sizeof(uint8_t) * n_size);
        for (;p_num > 0; p_num--) {
           networks[0][p_num-1] = p_num;
        }
        *n_num = 1;
        return networks;
    }

    uint64_t network_comb_count = choose(p_num, n_size);
    printf("There are %" PRIu64 " possible networks of size %"
                    PRIu8 " with %" PRIu8 " players.\n",
                    network_comb_count, n_size, p_num);

    // construct list of all possible networks
    printf("\tConstructing all possible networks...\n");
    NETWORK *all_possible = malloc(network_comb_count*sizeof(NETWORK));
    uint64_t n;
    for (n = 0; n < network_comb_count; n++) {
        all_possible[n] = malloc(sizeof(uint8_t) * n_size);
    }
    NETWORK buf = malloc(sizeof(uint8_t) * n_size);
    uint64_t c_idx = 0;
    compute_combinations(all_possible, buf, &c_idx, 1, p_num, 0, n_size);
    free(buf);

    // greedily choose networks to cover the maximum number of pairs
    // until all pairs are covered
    uint64_t total_pair_count = choose(p_num, 2);
    printf("\tThere are %" PRIu64 " unique pairs to cover.\n",total_pair_count);

    // use this to mark which networks are used in the cover
    // it would be way more space efficient to use a linked list, but whatever
    uint8_t *network_used_flag =  malloc(sizeof(uint8_t) * network_comb_count);
    uint64_t nuf_idx;
    for (nuf_idx = 0; nuf_idx < network_comb_count; nuf_idx++) {
        network_used_flag[nuf_idx] = 0;
    }
    // always choose the first network first
    PAIR *covered_pairs  = get_network_pairs(all_possible[0], n_size);
    uint64_t ppn = choose(n_size, 2), pair_count = choose(n_size, 2);
    uint64_t network_count = 1;
    network_used_flag[0] = 1;

    printf("\t%" PRIu64 " network used to cover %" PRIu64 " pairs.\n",
                    network_count, pair_count);

    while (pair_count < total_pair_count) {
        // find the network with the maximum number of uncovered piars
        uint64_t max_new_pairs = 0, max_idx = 0, idx;
        for (idx = 1; idx < network_comb_count; idx++) {
            if (network_used_flag[idx]) continue; // this is already used
            PAIR *c_pairs = get_network_pairs(all_possible[idx], n_size);
            uint64_t c_new_pairs = count_new_pairs(covered_pairs, pair_count,
                            c_pairs, ppn);
            free(c_pairs);

            if (c_new_pairs > max_new_pairs) {
                max_new_pairs = c_new_pairs;
                max_idx = idx;
            }
        }
        if (max_idx == 0) break; // no new pairs found

        // add it to the result
        PAIR *nn_pairs = get_network_pairs(all_possible[max_idx], n_size);
        PAIR *tmp = merge_pair_list(covered_pairs, pair_count, nn_pairs, ppn);
        free(nn_pairs); free(covered_pairs);

        covered_pairs = tmp;
        network_count++;
        pair_count += max_new_pairs;
        network_used_flag[max_idx] = 1;

        printf("\t%" PRIu64 " networks used to cover %" PRIu64 " pairs.\n",
                         network_count, pair_count);
    }

    // prepare and return the result
    *n_num = network_count;
    NETWORK *result = malloc(sizeof(NETWORK) * network_count);
    uint64_t r_idx, a_idx;
    for (r_idx = 0, a_idx = 0; a_idx < network_comb_count; a_idx++) {
        if (network_used_flag[a_idx]) {
            result[r_idx++] = all_possible[a_idx];
        }
    }
    free(all_possible);
    free(network_used_flag);
    return result;
}

uint64_t choose(uint64_t n, uint64_t k) {
    // Source: http://stackoverflow.com/questions/1838368/calculating-
    // the-amount-of-combinations

    if (k > n) {
        // invalid arguments.. who cares about errors?
        return 0;
    }
    uint64_t r = 1;
    uint64_t d;
    for (d = 1; d <= k; d++, n--) {
        uint64_t g = gcd(r, d);
        r /= g;
        r *= n / (d / g);
    }
    return r;
}

uint64_t gcd(uint64_t x, uint64_t y) {
    while (y != 0) {
        uint64_t temp = x % y;
        x = y;
        y = temp;
    }
    return x;
}

void compute_combinations(NETWORK *all, NETWORK buf, uint64_t *c_idx,
                uint8_t start, uint8_t end, uint8_t index, uint8_t r) {
    // Holy shit this actually works...
    if (index == r) {
        memcpy(all[*c_idx], buf, sizeof(uint8_t) * r);
        *c_idx += 1;
        return;
    }
    uint8_t i;
    for (i=start; i<=end && end-i+1 >= r-index; i++) {
        buf[index] = i;
        compute_combinations(all, buf, c_idx, i+1, end, index+1, r);
    }
}

PAIR *get_network_pairs(NETWORK network, uint8_t n_size) {
    uint8_t i, j;
    uint64_t p_idx = 0;
    PAIR *n_pairs = malloc(choose(n_size, 2) * sizeof(PAIR));
    for (i = 0; i < n_size - 1; i++) {
        for (j = i + 1; j < n_size; j++) {
            n_pairs[p_idx++] = (PAIR){ network[i], network[j] };
        }
    }
    return n_pairs;
}

PAIR *merge_pair_list(PAIR *a, uint64_t a_size, PAIR *b, uint64_t b_size) {

    PAIR *result = malloc((a_size + b_size) * sizeof(PAIR));
    uint64_t a_idx = 0, b_idx = 0, r_idx = 0;

    while (a_idx < a_size && b_idx < b_size) {
        int8_t cmp = compare_pairs(a[a_idx], b[b_idx]);
        if (cmp == -1) {
            result[r_idx++] = a[a_idx++];
        } else {
            result[r_idx++] = b[b_idx++];
            if (cmp == 0) a_idx++; // no duplicates in new list
        }
    }
    if (a_idx == a_size) {
        while (b_idx < b_size) {
            result[r_idx++] = b[b_idx++];
        }
    } else {
        while (a_idx < a_size) {
            result[r_idx++] = a[a_idx++];
        }
    }
    return result;
}

uint64_t count_new_pairs(PAIR *src, uint64_t src_size,
                PAIR *new_p, uint64_t new_size) {

    uint64_t new_idx = 0, src_idx = 0, result = 0;
    while (src_idx < src_size && new_idx < new_size) {
        switch (compare_pairs(src[src_idx], new_p[new_idx])) {
            case 0:
                src_idx++;
                new_idx++;
                break;
            case -1:
                src_idx++;
                break;
            case 1:
                new_idx++;
                result++;
                break;
        }
    }
    return result + (new_size - new_idx);
}

int8_t compare_pairs(PAIR a, PAIR b) {
    if (a.a == b.a && a.b == b.b) return 0;
    if (a.a == b.a) {
        return a.b < b.b ? -1 : 1;
    }
    return a.a < b.a ? -1 : 1;
}
